/**
 * @file
 * Initialize Marker.io service.
 */

"use strict";

(function (window, drupalSettings) {
  // Initialize variables.
  var destination;
  var userEmail;
  var userName;
  var nid;

  // Get Marker.io destination.
  destination = drupalSettings.markerio.destination;

  // Get e-mail and username of authenticated user, if it's available.
  if (drupalSettings.markerio.user_email) {
    userEmail = drupalSettings.markerio.user_email;
  }
  if (drupalSettings.markerio.user_name) {
    userName = drupalSettings.markerio.user_name;
  }
  if (drupalSettings.markerio.nid) {
    nid = drupalSettings.markerio.nid;
  }

  // Initialize Marker.io.
  window.markerConfig = {
    destination: destination,
    reporter: {
      email: userEmail,
      fullName: userName,
    },
    customData: {
      nid: nid
    }
  };

  !function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);

})(window, drupalSettings);
