# Marker.io

This module allows you to collect visual website feedback from visitors and
users directly in your website using Marker.io

## Requirements
An active [Marker.io](https://marker.io/?via=oum-drupal) subscription.

## Installation
Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/docs/extending-drupal Installing modules for
further information.

```shell
composer require drupal/markerio
drush en markerio
```

## Configuration
- Go to the 'Marker.io' configuration page: `/admin/config/system/markerio`,
  and enter your destination key.
- Go to the permission page and set up which roles are allowed to
  `Access Marker.io widget`.
